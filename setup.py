from setuptools import setup, find_packages

# To use a consistent encoding
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()


setup(
    name="vaix-django-extensions",
    version='1.0.1',
    maintainer='VAIX.AI',
    maintainer_email='pn@vaix.ai',
    license="MIT-LICENSE",
    url='https://gitlab.com/pnezis/vaix-django-extensions',
    platforms=["any"],
    description="Django helper classes",
    long_description=long_description,
    packages=find_packages(exclude=[]),
    package_data={'': ['MIT-LICENSE']},
    include_package_data=True,
    install_requires=['django', 'django-url-filter', 'djangorestframework'],
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 2',
    ]
)
