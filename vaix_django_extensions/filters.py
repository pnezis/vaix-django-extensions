from url_filter.backends.django import DjangoFilterBackend


class FilterBackendWithoutDistinct(DjangoFilterBackend):

    def filter_by_specs(self, queryset):
        """
        Filter queryset by applying all filter specifications

        The filtering is done by calling ``QuerySet.filter`` and
        ``QuerySet.exclude`` as appropriate.
        """
        include = {self._prepare_spec(i): i.value for i in self.includes}
        exclude = {self._prepare_spec(i): i.value for i in self.excludes}

        if include:
            queryset = queryset.filter(**include)
        if exclude:
            queryset = queryset.exclude(**exclude)

        return queryset
