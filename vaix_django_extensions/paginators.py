from rest_framework.pagination import LimitOffsetPagination, _get_count


class LimitOffsetCachedPagination(LimitOffsetPagination):
    def paginate_queryset(self, queryset, request, view=None):
        self.count = self._cached_count(queryset, request)
        self.limit = self.get_limit(request)
        print self.limit
        if self.limit is None:
            return None

        self.offset = self.get_offset(request)
        self.request = request
        if self.count > self.limit and self.template is not None:
            self.display_page_controls = True

        if self.count == 0 or self.offset > self.count:
            return []
        return list(queryset[self.offset:self.offset + self.limit])

    def _cached_count(self, queryset, request):
        from django.core.cache import cache

        cache_key = request.path
        skip_keys = ['offset', 'limit']
        filter_query = [key+value for key, value in request.GET.iteritems()
                        if key not in skip_keys]
        cache_key += '_'.join(filter_query)

        count = cache.get(cache_key)

        if count is None:
            count = _get_count(queryset)
            cache.set(cache_key, count, None)
        return count
